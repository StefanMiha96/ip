let host = {};

host['clk.sh'] = {};

host['clk.sh']['Greenland'] = {};
host['clk.sh']['Greenland'].mobile = 0.02;
host['clk.sh']['Greenland'].desktop = 0.02;

host['clk.sh']['Iceland'] = {};
host['clk.sh']['Iceland'].mobile = 0.013;
host['clk.sh']['Iceland'].desktop = 0.013;

host['clk.sh']['San Marino'] = {};
host['clk.sh']['San Marino'].mobile = 0.01;
host['clk.sh']['San Marino'].desktop = 0.01;

host['clk.sh']['United States'] = {};
host['clk.sh']['United States'].mobile = 0.009;
host['clk.sh']['United States'].desktop = 0.009;

host['clk.sh']['United Kingdom'] = {};
host['clk.sh']['United Kingdom'].mobile = 0.09;
host['clk.sh']['United Kingdom'].desktop = 0.09;

host['clk.sh']['Andorra'] = {};
host['clk.sh']['Andorra'].mobile = 0.08;
host['clk.sh']['Andorra'].desktop = 0.08;

host['clk.sh']['New Zealand'] = {};
host['clk.sh']['New Zealand'].mobile = 0.07;
host['clk.sh']['New Zealand'].desktop = 0.07;

host['clk.sh']['Gibraltar'] = {};
host['clk.sh']['Gibraltar'].mobile = 0.07;
host['clk.sh']['Gibraltar'].desktop = 0.07;

host['clk.sh']['Australia'] = {};
host['clk.sh']['Australia'].mobile = 0.07;
host['clk.sh']['Australia'].desktop = 0.07;

host['clk.sh']['Canada'] = {};
host['clk.sh']['Canada'].mobile = 0.07;
host['clk.sh']['Canada'].desktop = 0.07;

host['clk.sh']['Germany'] = {};
host['clk.sh']['Germany'].mobile = 0.06;
host['clk.sh']['Germany'].desktop = 0.06;

host['clk.sh']['France'] = {};
host['clk.sh']['France'].mobile = 0.06;
host['clk.sh']['France'].desktop = 0.06;

host['clk.sh']['Italy'] = {};
host['clk.sh']['Italy'].mobile = 0.05;
host['clk.sh']['Italy'].desktop = 0.05;

host['clk.sh']['Sweden'] = {};
host['clk.sh']['Sweden'].mobile = 0.05;
host['clk.sh']['Sweden'].desktop = 0.05;

host['clk.sh']['Norway'] = {};
host['clk.sh']['Norway'].mobile = 0.05;
host['clk.sh']['Norway'].desktop = 0.05;

host['clk.sh']['Spain'] = {};
host['clk.sh']['Spain'].mobile = 0.05;
host['clk.sh']['Spain'].desktop = 0.05;

host['clk.sh']['Denmark'] = {};
host['clk.sh']['Denmark'].mobile = 0.05;
host['clk.sh']['Denmark'].desktop = 0.05;

host['clk.sh']['Finland'] = {};
host['clk.sh']['Finland'].mobile = 0.05;
host['clk.sh']['Finland'].desktop = 0.05;

host['clk.sh']['Else'] = {};
host['clk.sh']['Else'].mobile = 0.003;
host['clk.sh']['Else'].desktop = 0.003;


host['sh.st'] = {};

host['sh.st']['Greenland'] = {};
host['sh.st']['Greenland'].mobile = 0.02;
host['sh.st']['Greenland'].desktop = 0.02;

host['sh.st']['Iceland'] = {};
host['sh.st']['Iceland'].mobile = 0.02;
host['sh.st']['Iceland'].desktop = 0.02;

host['sh.st']['San Marino'] = {};
host['sh.st']['San Marino'].mobile = 0.02;
host['sh.st']['San Marino'].desktop = 0.02;

host['sh.st']['United States'] = {};
host['sh.st']['United States'].mobile = 0.014;
host['sh.st']['United States'].desktop = 0.014;

host['sh.st']['United Kingdom'] = {};
host['sh.st']['United Kingdom'].mobile = 0.01;
host['sh.st']['United Kingdom'].desktop = 0.01;

host['sh.st']['Andorra'] = {};
host['sh.st']['Andorra'].mobile = 0.02;
host['sh.st']['Andorra'].desktop = 0.02;

host['sh.st']['New Zealand'] = {};
host['sh.st']['New Zealand'].mobile = 0.02;
host['sh.st']['New Zealand'].desktop = 0.02;

host['sh.st']['Gibraltar'] = {};
host['sh.st']['Gibraltar'].mobile = 0.02;
host['sh.st']['Gibraltar'].desktop = 0.02;

host['sh.st']['Australia'] = {};
host['sh.st']['Australia'].mobile = 0.08;
host['sh.st']['Australia'].desktop = 0.08;

host['sh.st']['Canada'] = {};
host['sh.st']['Canada'].mobile = 0.05;
host['sh.st']['Canada'].desktop = 0.05;

host['sh.st']['Germany'] = {};
host['sh.st']['Germany'].mobile = 0.07;
host['sh.st']['Germany'].desktop = 0.07;

host['sh.st']['France'] = {};
host['sh.st']['France'].mobile = 0.048;
host['sh.st']['France'].desktop = 0.048;

host['sh.st']['Switzerland'] = {};
host['sh.st']['Switzerland'].mobile = 0.06;
host['sh.st']['Switzerland'].desktop = 0.06;

host['sh.st']['Slovenia'] = {};
host['sh.st']['Slovenia'].mobile = 0.024;
host['sh.st']['Slovenia'].desktop = 0.024;

host['sh.st']['Slovakia'] = {};
host['sh.st']['Slovakia'].mobile = 0.024;
host['sh.st']['Slovakia'].desktop = 0.024;

host['sh.st']['Bahrain'] = {};
host['sh.st']['Bahrain'].mobile = 0.02;
host['sh.st']['Bahrain'].desktop = 0.02;

host['sh.st']['Portugal'] = {};
host['sh.st']['Portugal'].mobile = 0.024;
host['sh.st']['Portugal'].desktop = 0.024;

host['sh.st']['Italy'] = {};
host['sh.st']['Italy'].mobile = 0.05;
host['sh.st']['Italy'].desktop = 0.05;

host['sh.st']['Sweden'] = {};
host['sh.st']['Sweden'].mobile = 0.069;
host['sh.st']['Sweden'].desktop = 0.069;

host['sh.st']['Norway'] = {};
host['sh.st']['Norway'].mobile = 0.08;
host['sh.st']['Norway'].desktop = 0.08;

host['sh.st']['Spain'] = {};
host['sh.st']['Spain'].mobile = 0.0035;
host['sh.st']['Spain'].desktop = 0.0035;

host['sh.st']['Mexico'] = {};
host['sh.st']['Mexico'].mobile = 0.0009;
host['sh.st']['Mexico'].desktop = 0.0009;

host['sh.st']['Argentina'] = {};
host['sh.st']['Argentina'].mobile = 0.0006;
host['sh.st']['Argentina'].desktop = 0.0006;

host['sh.st']['Colombia'] = {};
host['sh.st']['Colombia'].mobile = 0.0006;
host['sh.st']['Colombia'].desktop = 0.0006;

host['sh.st']['Brazil'] = {};
host['sh.st']['Brazil'].mobile = 0.0014;
host['sh.st']['Brazil'].desktop = 0.0014;

host['sh.st']['Poland'] = {};
host['sh.st']['Poland'].mobile = 0.004;
host['sh.st']['Poland'].desktop = 0.004;

host['sh.st']['Saudi Arabia'] = {};
host['sh.st']['Saudi Arabia'].mobile = 0.003;
host['sh.st']['Saudi Arabia'].desktop = 0.003;

host['sh.st']['Netherlands'] = {};
host['sh.st']['Netherlands'].mobile = 0.0055;
host['sh.st']['Netherlands'].desktop = 0.0055;

host['sh.st']['Qatar'] = {};
host['sh.st']['Qatar'].mobile = 0.005;
host['sh.st']['Qatar'].desktop = 0.005;

host['sh.st']['Lebanon'] = {};
host['sh.st']['Lebanon'].mobile = 0.0028;
host['sh.st']['Lebanon'].desktop = 0.0028;

host['sh.st']['Latvia'] = {};
host['sh.st']['Latvia'].mobile = 0.0026;
host['sh.st']['Latvia'].desktop = 0.0026;

host['sh.st']['Denmark'] = {};
host['sh.st']['Denmark'].mobile = 0.09;
host['sh.st']['Denmark'].desktop = 0.09;

host['sh.st']['Finland'] = {};
host['sh.st']['Finland'].mobile = 0.07;
host['sh.st']['Finland'].desktop = 0.07;

host['sh.st']['Else'] = {};
host['sh.st']['Else'].mobile = 0.003;
host['sh.st']['Else'].desktop = 0.003;



host['exe.io'] = {};

host['exe.io']['United States'] = {};
host['exe.io']['United States'].mobile = 0.012;
host['exe.io']['United States'].desktop = 0.012;

host['exe.io']['United Kingdom'] = {};
host['exe.io']['United Kingdom'].mobile = 0.008;
host['exe.io']['United Kingdom'].desktop = 0.008;

host['exe.io']['Australia'] = {};
host['exe.io']['Australia'].mobile = 0.0085;
host['exe.io']['Australia'].desktop = 0.0085;

host['exe.io']['Canada'] = {};
host['exe.io']['Canada'].mobile = 0.01;
host['exe.io']['Canada'].desktop = 0.01;

host['exe.io']['Germany'] = {};
host['exe.io']['Germany'].mobile = 0.01;
host['exe.io']['Germany'].desktop = 0.01;

host['exe.io']['France'] = {};
host['exe.io']['France'].mobile = 0.0065;
host['exe.io']['France'].desktop = 0.0065;

host['exe.io']['United Arab Emirates '] = {};
host['exe.io']['United Arab Emirates '].mobile = 0.011;
host['exe.io']['United Arab Emirates '].desktop = 0.011;

host['exe.io']['Italy'] = {};
host['exe.io']['Italy'].mobile = 0.05;
host['exe.io']['Italy'].desktop = 0.05;

host['exe.io']['Sweden'] = {};
host['exe.io']['Sweden'].mobile = 0.009;
host['exe.io']['Sweden'].desktop = 0.009;

host['exe.io']['Italy'] = {};
host['exe.io']['Italy'].mobile = 0.005;
host['exe.io']['Italy'].desktop = 0.005;

host['exe.io']['Spain'] = {};
host['exe.io']['Spain'].mobile = 0.006;
host['exe.io']['Spain'].desktop = 0.006;

host['exe.io']['Saudi Arabia'] = {};
host['exe.io']['Saudi Arabia'].mobile = 0.005;
host['exe.io']['Saudi Arabia'].desktop = 0.005;

host['exe.io']['Netherlands'] = {};
host['exe.io']['Netherlands'].mobile = 0.007;
host['exe.io']['Netherlands'].desktop = 0.007;

host['exe.io']['Qatar'] = {};
host['exe.io']['Qatar'].mobile = 0.007;
host['exe.io']['Qatar'].desktop = 0.007;

host['exe.io']['Else'] = {};
host['exe.io']['Else'].mobile = 0.0035;
host['exe.io']['Else'].desktop = 0.0035;
// Greenland	$20.000	$20.000
// Iceland	$13.000	$13.000
// San Marino	$10.000	$10.000
// United States	$9.000	$9.000
// United Kingdom	$8.000	$8.000
// Andorra	$8.000	$8.000
// New Zealand	$7.000	$7.000
// Gibraltar	$7.000	$7.000
// Australia	$7.000	$7.000
// Canada	$7.000	$7.000
// Germany	$6.000	$6.000
// France	$6.000	$6.000
// Denmark	$5.000	$5.000
// Spain	$5.000	$5.000
// Italy	$5.000	$5.000
// Sweden	$5.000	$5.000
// Norway	$5.000	$5.000
// Thailand	$4.000	$4.000
// Brazil	$4.000	$4.000
// Finland	$4.000	$4.000
// Philippines










module.exports.data = host;





