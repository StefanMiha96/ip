let express = require('express');
let mysql = require('mysql');
let util = require('util');
let router = express.Router();
let rp = require('request-promise');

const {promisify} = require('util');
const redis = require("redis");
const client = redis.createClient();
// let publisher = redis.createClient();
// let subscriber = redis.createClient();
const getAsync = promisify(client.get).bind(client);
const setAsync = promisify(client.set).bind(client);

let apiKeyObject = {};

let pool = mysql.createPool({
  host: '127.0.0.1',
  user: 'root',
  password: 'Volimdaubijam1+',
  database: 'ip',
  connectionLimit: 21,
  supportBigNumbers: true,
  bigNumberStrings: true
});

pool.query = util.promisify(pool.query);

// subscriber.on('message', function (channel, message) {
//   console.log('Message: ' + message + ' on channel: ' + channel + ' is arrive!');
// });
// subscriber.subscribe('notification');



router.get('/data', async function (req, res) {

  //if(apiKeyObject)
  let innerData = JSON.parse(await getAsync('apiKeyObject'));
  if(innerData){
    apiKeyObject = innerData;
  }
  
  //console.log(await getAsync('apiKeyObject'));

  if (!apiKeyObject[req.query.api_key]) {
    let data = await pool.query('SELECT u.*,t.query_num FROM ip.user u INNER JOIN ip.tiers t on u.tier = t.id where u.api_key = ?', [req.query.api_key]);
    if (data[0]) {
      apiKeyObject[req.query.api_key] = {};
      apiKeyObject[req.query.api_key] = {};
      apiKeyObject[req.query.api_key] = JSON.parse(data[0].user_data);
      apiKeyObject[req.query.api_key].api_key = req.query.api_key;
    } else if (req.query.api_key == 'trial') {
      apiKeyObject[req.query.api_key] = {};
      apiKeyObject[req.query.api_key] = {
        trial: true,
        resetTimer: new Date().getTime() + 86400000,
        usedTimes: 1,
      };
      apiKeyObject[req.query.api_key].query_num = 5000;
    } else {
      return res.send('not valid api_key');
    }
  }
  if (apiKeyObject[req.query.api_key]) {
    return res.send(await getIpHack(req, res));
  } else {
    return res.send('not valid request');
  }

});

async function getIpHack(req, res) {
  //apiKeyObject[req.query.api_key].canUse = await updateUserStats(req);
  
  if (!await updateUserStats(req)) {
    return res.send('not valid request');
  }

  let ipData = await pool.query('SELECT * FROM ip.ip where ip = ?', [req.query.ip]);
  if (ipData[0]) {
    //JSON.parse(ipData[0]);
    return ipData[0].data;
  }

  try {
    ipData = await rp(await getIpHeader(req.query.ip));
    pool.query('INSERT INTO ip.ip values(null, ?,?,?)', [req.query.ip, ipData.body, new Date().getTime()]);
    return ipData.body;
  } catch (err) {
    return 'false ip';
  }



}

async function getIpHeader(ip) {

  let options = {
    method: 'GET',
    uri: `https://ipinfo.io/widget/${ip}`,
    headers: {
      'referer': 'https://ipinfo.io/',
      'X-Forwarded-For': `${ip}`,
      'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36',
    },
    resolveWithFullResponse: true
  }

  return options

}

async function updateUserStats(req) {
  if (!apiKeyObject[req.query.api_key]) {
    apiKeyObject[req.query.api_key] = {};
    apiKeyObject[req.query.api_key].usedTimes = 1;
    apiKeyObject[req.query.api_key].tillRestart = 1;
  } else {
    apiKeyObject[req.query.api_key].usedTimes++;
    apiKeyObject[req.query.api_key].tillRestart++;
  }

  if (apiKeyObject[req.query.api_key].tillRestart > 5200 && !apiKeyObject[req.query.api_key].trial) {
    apiKeyObject[req.query.api_key].tillRestart = 0;
    pool.query('UPDATE ip.user SET user_data = ? where api_key = ?', [JSON.stringify(apiKeyObject[req.query.api_key]), req.query.api_key]);
  }

  if (apiKeyObject[req.query.api_key].resetTimer < new Date().getTime()) {
    apiKeyObject[req.query.api_key].usedTimes = 0;
  }
  // publisher.publish('notification', JSON.stringify(apiKeyObject[req.query.api_key]), function () {

  // });

  await setAsync ('apiKeyObject',JSON.stringify(apiKeyObject));
  //client.set("apiKeyObject", JSON.stringify(apiKeyObject));

  if (apiKeyObject[req.query.api_key].usedTimes > apiKeyObject[req.query.api_key].query_num) {
    return false;
  } else {
    return true;
  }
}


let wait = ms => new Promise((r, j) => setTimeout(r, ms));


module.exports = router;
