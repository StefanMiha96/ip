var express = require('express');
var path = require('path');
var https = require('https');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');


var routes = require('./server/routes/indexRouter');
var cors = require('cors');
var session = require('express-session');
var passport = require('passport');
var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.set('env', 'development');
app.enable('trust proxy')

// var config = require('./server/database/config.json')[app.get('env')];
// require('./server/config/passport')(config);

app.set('port', process.env.PORT || 1000);
//app.use(logger('dev'));
app.use(bodyParser.json());

// app.use(function(req, res, next) {
//     //res.setHeader('Content-Type', 'application/json');
//     //res.header("Access-Control-Allow-Origin", "*");
//    // res.header("Access-Control-Allow-Origin", "*","Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, application/json");
//     next();
//   });

app.use(bodyParser.urlencoded({ extended: false }));
//app.use(cookieParser());
//app.use(express.static(path.join(__dirname, 'client')));
//app.use(express.static(config.imageRepo));

// app.use(session({secret: 'linkMe',
//     resave:true,
//     saveUninitialized:true,
//     cookie: { maxAge : 365 * 24 * 60 * 60 * 1000 }}));
// app.use(passport.initialize());
// app.use(passport.session());

// app.use(cors());

// app.get('/*', function(req, res, next){ 
//     res.setHeader('Last-Modified', (new Date()).toUTCString());
//     next(); 
//   });
  
app.use('/', routes);




// app.disable('etag');

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers

// development error handler
// will print stacktrace
// if (app.get('env') === 'development') {
//     app.use(function(err, req, res, next) {
//         res.status(err.status || 500);
//         res.json(err);
//     });
// }

// httpProxy.createProxyServer({target:'http://localhost:3000'}).listen(25565);

// var selfsigned = require('selfsigned');
// var attrs = [{ name: 'commonName', value: 'contoso.com' }];
// var pems = selfsigned.generate(attrs, { days: 365 });
// console.log(pems)

// var options = {
//     key: pems.private,
//     cert: pems.cert
// };

// https.createServer(options, app).listen(443);



var server = app.listen(app.get('port'), function() {
    
  console.log('Express server listening on port ' + server.address().port);
 // console.log = function(){}
});




